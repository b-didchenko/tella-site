(function($) {
    "use strict";

    // Internet Explorer 10 in Windows Phone 8 viewport bug
    if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
        var msViewportStyle = document.createElement('style');
        msViewportStyle.appendChild(
            document.createTextNode(
                '@-ms-viewport{width:auto!important}'
            )
        );
        document.head.appendChild(msViewportStyle);
    }

    // Check if element is in viewport
    $.fn.isInViewport = function() {
        var elementTop    = $(this).offset().top;
        var elementBottom = elementTop + $(this).outerHeight();

        var viewportTop    = $(window).scrollTop();
        var viewportBottom = viewportTop + $(window).height();

        return elementBottom > viewportTop && elementTop < viewportBottom;
    };

    function showNav(target, that) {
        const nav = $(target);

        $('body, html').toggleClass('modal-opened');
        $(that).toggleClass('active');
        nav.toggleClass('active');
    }

    function fixedNav(header) {
        if($(window).scrollTop() > 1) {
            header.addClass('fixed');
            header.find('svg').removeClass('white');
        } else {
            header.removeClass('fixed');
            header.find('svg').addClass('white');
        }
    }

    function verticalAlign(target) {

        target.each((index, item) => {

            var height = $(item).outerHeight() / 2,
                offset = parseInt($(item).closest('.section').css('padding-top'));

            $(item).css({
                'margin-top': `-${(height + offset)}px`
            });
        });
    }

    function scrollTo(target) {
        $('body, html').animate({
            scrollTop: $(target).offset().top - $('.top-header').outerHeight()
        }, 1000).promise().then(function() {
            $(target).find('.accordion-item__toggle').trigger('click')
        });
        return false;
    }

    $(document).ready(function () {

        var isMob = /Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);

        // Load custom fonts
        const font = new FontFaceObserver('Signika');
        font.load().then(function () {
          $('html').addClass('fonts-loaded');
        });

        // Фиксация меню при скролле
        const $header = $('.top-header');
        enquire.register("screen and (min-width: 768px)", {
            setup () {
                $header.find('svg').removeClass('white');
            },
            match () {
                $header.find('svg').addClass('white');
                $(window).on('scroll', () => fixedNav($header));
            },
            unmatch () {
                $header.find('svg').removeClass('white');
            }
        }, true);


        // Кнопка 'Вверх'
        const topBtn = $('.top-btn');
        topBtn.on('click', () => {
            $('body,html').stop().animate({
                scrollTop: 0
            }, 'slow')
        });

        const $navTrigger = $('.nav-trigger');
        $navTrigger.on('click', (e) => showNav('.nav', e.target));

        $(document).on('click','.accordion-item__toggle', function () {
            $(this).toggleClass('active');
            $(this).closest('.accordion-item').find('.accordion-item__hidden').stop().slideToggle();
            return false;
        })

        $(".fancybox-popup").fancybox();


        const header = $('.app-header');
        header.length && setTimeout(()=>{verticalAlign(header)}, 0);

        $(window).on('orientationchange', () => {
            header.length && setTimeout(()=>{verticalAlign(header)}, 0);
        });

        const backgroundColor = $('.header-bg-inner').css('color');
        $('.footer').css('background', backgroundColor)

        const link = window.location.hash;

        if (link.length) {
            window.scrollTo(0, 0);
            scrollTo(link);
        }

    });

})(jQuery);
